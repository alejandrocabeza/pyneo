__author__ = 'Strinnityk'

from Connector import Connector
from py2neo import Node
from py2neo import Relationship

class Vehicle:

    ids = 0

    @staticmethod
    def create(origin, destination, type):
        conn = Connector()

        vehicle = Node("Vehicle", name=type)
        vehicle["id"] = Vehicle.ids
        vehicle["onservice"] = "false"

        statement = "match (c:City{name:'%s'}) return c"%origin
        statement = "match (c:City{name:'%s'}) return c"%destination
        orCity  = conn.client.find_one("City", "name", origin)
        desCity = conn.client.find_one("City", "name", destination)


        conn.client.create(
            vehicle,
            Relationship(orCity, "From", vehicle),
            Relationship(vehicle, "To", desCity)
        )

        Vehicle.ids += 1
        return vehicle

    @staticmethod
    def get(origin, destination, type):
        conn = Connector()
        statement = "match (c:City{name:'%s'})-[r:From]-(v:Vehicle{name:'%s'})-[q:To]-(d:City{name:'%s'}) return v"%(origin, type, destination)
        return conn.client.cypher.execute_one(statement)

    @staticmethod
    def exists(origin, destination, type):
        if Vehicle.get(origin, destination,type):
            return True
        return False

    @staticmethod
    def canShip(origin, destination, type):
        vehicle = Vehicle.get(origin, destination,type)
        if vehicle:
            if vehicle["onservice"] == True:
                return False
            else:
                return True
        return None

    @staticmethod
    def launch(origin, destination, type):
        conn = Connector()
        vehicle = Vehicle.get(origin, destination, type)
        if vehicle:
            statement = "match (p:Package)-[r:Carries]-(v:Vehicle) where (:City{name:'%s'})-[:From]-(v)-[:To]-(:City{name:'%s'}) return r"%(origin, destination)
            packages = conn.client.cypher.execute(statement)
            print len(packages)
            if len(packages) > 0:
                vehicle["onservice"] = True
                vehicle.push()
                return True
        return False

    @staticmethod
    def finish(origin, destination, type):
        vehicle = Vehicle.get(origin, destination, type)
        if vehicle:
            vehicle["onservice"] = False
            vehicle.push()
            return True
        return False



if __name__ == "__main__":
    print "Vehicle"
    #Vehicle.create("Santiago", "Madrid", "aereo")
    #print Vehicle.get("Santiago", "Madrid", "aereo")
    #print Vehicle.exists("Santiago", "Madrid", "aereo")
    #print Vehicle.canShip("Santiago", "Madrid", "aereo")
    #print Vehicle.launch("Santiago", "Madrid", "aereo")
    #print Vehicle.finish("Santiago", "Madrid", "aereo")

