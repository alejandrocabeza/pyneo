__author__ = 'manuel.cabeza'

from Connector import Connector
from Vehicle import Vehicle
from Package import Package
from Client import Client

from py2neo import Node
from py2neo import Relationship

import py2neo
import sys

class Courier:

    @staticmethod
    def getRoute(origin, destination, service):
        conn = Connector()

        timestatement = "match (s:Service{name:'%s'}) return s"%service
        maxtime = conn.client.cypher.execute_one(timestatement)["maxtime"]

        statement = "match (o:City{name:'%s'})-[r]->(d:City{name:'%s'}) where r.time < %s return r"%(origin, destination, sys.maxint if maxtime == 0 else maxtime )
        relations = conn.client.cypher.execute(statement)

        if maxtime == 0:
            minprice = sys.maxint
            minroute = None
            print len(relations)
            for rel in relations:
                travelprice = rel.r._Path__rels[0]["price"]
                if  minprice > travelprice:
                    minprice = travelprice
                    minroute = rel

            return minroute
        else:
            return relations

    @staticmethod
    def ship(origin, destination, type, service, clientname, clientdni):
        conn = Connector()
        package = Package.createPackage(origin, destination, service)
        vehicle = None
        client = None

        canShip = Vehicle.canShip(origin, destination, type)
        if canShip == None:
            vehicle = Vehicle.create(origin, destination, type)
        elif canShip == True:
            vehicle = Vehicle.get(origin, destination, type)
        else:
            return False

        clientExists = Client.clientExists(clientname, clientdni)
        if clientExists == False:
            client = Client.createClient(clientname, clientdni)
        client = Client.getClient(clientname, clientdni)

        conn.client.create(
            Relationship(vehicle, "Carries", package),
            Relationship(package, "Owns", client)
        )



if __name__ == "__main__":
    print "API"
    #Courier.ship("Santiago", "San Sebastian", "aereo", "economico", "pepe da chousa", "4444")
    #print Courier.getRoute("Santiago", "Madrid", "normal")