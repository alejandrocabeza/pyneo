__author__ = 'Strinnityk'

from Connector import Connector
from py2neo import Node

class Client:

    @staticmethod
    def createClient(name, dni):
        conn = Connector()
        client = Node("Client", name=name)
        client["dni"] = dni
        conn.client.create(client)

    @staticmethod
    def getClient(name, dni):
        conn = Connector()
        statement = "match (c:Client{name:'%s'}) where c.dni='%s' return c"%(name, dni)
        return conn.client.cypher.execute_one(statement)

    @staticmethod
    def clientExists(name, dni):
        if Client.getClient(name, dni):
            return True
        return False

    @staticmethod
    def getServicesHired(name, dni, services=[]):
        conn = Connector()
        statement = "match (p:Package)-[r:Owns]-(c:Client) where c.name='%s' and c.dni='%d' and p.service in %s return p as Shipment"%(name, dni, services)
        packages = conn.client.cypher.execute(statement)
        return packages

    @staticmethod
    def getNonPaidServices(name, dni):
        conn = Connector()
        statement = "match (p:Package)-[r:Owns]-(c:Client) where c.name='%s' and c.dni='%d' and p.paid=False return p"%(name, dni)
        packages = conn.client.cypher.execute(statement)
        return packages

    @staticmethod
    def getPaidServices(name, dni):
        conn = Connector()
        statement = "match (p:Package)-[r:Owns]-(c:Client) where c.name='%s' and c.dni='%d' and p.paid=True return p"%(name, dni)
        packages = conn.client.cypher.execute(statement)
        return packages

    @staticmethod
    def getServicesSummary(name, dni):
        conn = Connector()
        paidStatement = "match (p:Package)-[r:Owns]-(c:Client) where c.name='%s' and c.dni='%d' and p.paid=True return count(p) as PaidServices, sum(p.price) as Amount"%(name, dni)
        nonPaidStatement = "match (p:Package)-[r:Owns]-(c:Client) where c.name='%s' and c.dni='%d' and p.paid=False return count(p) as NonPaidServices, sum(p.price) as Amount"%(name, dni)
        print conn.client.cypher.execute(paidStatement)
        print conn.client.cypher.execute(nonPaidStatement)



if __name__ == "__main__":
    print "Client"
    #Client.createClient("pepe da chousa", 4444)
    #print Client.getClient("pepe da chousa", 4444)
    #print Client.getServicesHired("pepe da chousa", 4444, ["normal", "economico"])
    #print Client.clientExists("pepe da chousa", 4444)
    #print Client.getNonPaidServices("pepe da chousa", 4444)
    #print Client.getPaidServices("pepe da chousa", 4444)
    #Client.getServicesSummary("pepe da chousa", 4444)