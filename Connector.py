__author__ = 'manuel.cabeza'

from py2neo import neo4j
from py2neo import Node
from py2neo import Relationship


class Connector:

    def __init__(self):
        self.client = neo4j.Graph("http://localhost:7474/db/data/")

        if len(self.getAll()) == 0:
            self.initialize()

    def initialize(self):
        santiago     = Node("City", name = "Santiago")
        madrid       = Node("City", name = "Madrid")
        barcelona    = Node("City", name = "Barcelona")
        sevilla      = Node("City", name = "Sevilla")
        sansebastian = Node("City", name = "San Sebastian")

        urgentedia          = Node("Service", name = "urgentedia")
        urgentemadrugadores = Node("Service", name = "urgentemadrugadores")
        urgenteprimerahora  = Node("Service", name = "urgenteprimerahora")
        normal              = Node("Service", name = "normal")
        economico           = Node("Service", name = "economico")
        urgentedia         ["maxtime"] = 240
        urgentemadrugadores["maxtime"] = 360
        urgenteprimerahora ["maxtime"] = 480
        normal             ["maxtime"] = 720
        economico          ["maxtime"] = 0

        carretera = Node("Transport", name = "carretera")
        carretera ["timekm"]   = 60
        carretera ["timeload"] = 5
        carretera ["euroskm"]  = 1

        ferrocarril = Node("Transport", name = "ferrocarril")
        ferrocarril ["timekm"]   = 50
        ferrocarril ["timeload"] = 10
        ferrocarril ["euroskm"]  = 0.8

        aereo = Node("Transport", name = "aereo")
        aereo ["timekm"]   = 10
        aereo ["timeload"] = 40
        aereo ["euroskm"]  = 3.5

        maritimo = Node("Transport", name = "maritimo")
        maritimo ["timekm"]   = 120
        maritimo ["timeload"] = 20
        maritimo ["euroskm"]  = 0.3

        self.client.create(
            santiago,
            madrid,
            barcelona,
            sevilla,
            sansebastian,

            urgentedia,
            urgentemadrugadores,
            urgenteprimerahora,
            normal,
            economico,

            carretera,
            ferrocarril,
            aereo,
            maritimo,

            Relationship(santiago, ("Connected", {"distance" : 600,  "type":"carretera",   "time": (6   * carretera["timekm"]   + 2 * carretera["timeload"]),   "price": (6 * carretera["euroskm"])}),   madrid),
            Relationship(santiago, ("Connected", {"distance" : 600,  "type":"ferrocarril", "time": (6   * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), madrid),
            Relationship(santiago, ("Connected", {"distance" : 600,  "type":"aereo",       "time": (6   * aereo["timekm"]       + 2 * aereo["timeload"]),       "price": (6 * aereo["euroskm"])}),       madrid),
            Relationship(santiago, ("Connected", {"distance" : 1100, "type":"ferrocarril", "time": (11  * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), barcelona),
            Relationship(santiago, ("Connected", {"distance" : 1100, "type":"aereo",       "time": (11  * aereo["timekm"]       + 2 * aereo["timeload"]),       "price": (6 * aereo["euroskm"])}),       barcelona),
            Relationship(santiago, ("Connected", {"distance" : 1100, "type":"maritimo",    "time": (11  * maritimo["timekm"]    + 2 * maritimo["timeload"]),    "price": (6 * maritimo["euroskm"])}),    barcelona),
            Relationship(santiago, ("Connected", {"distance" : 810,  "type":"carretera",   "time": (8.1 * carretera["timekm"]   + 2 * carretera["timeload"]),   "price": (6 * carretera["euroskm"])}),   sevilla),
            Relationship(santiago, ("Connected", {"distance" : 810,  "type":"ferrocarril", "time": (8.1 * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), sevilla),
            Relationship(santiago, ("Connected", {"distance" : 810,  "type":"maritimo",    "time": (8.1 * maritimo["timekm"]    + 2 * maritimo["timeload"]),    "price": (6 * maritimo["euroskm"])}),    sevilla),
            Relationship(santiago, ("Connected", {"distance" : 660,  "type":"carretera",   "time": (6.6 * carretera["timekm"]   + 2 * carretera["timeload"]),   "price": (6 * carretera["euroskm"])}),   sansebastian),
            Relationship(santiago, ("Connected", {"distance" : 660,  "type":"aereo",       "time": (6.6 * aereo["timekm"]       + 2 * aereo["timeload"]),       "price": (6 * aereo["euroskm"])}),       sansebastian),
            Relationship(santiago, ("Connected", {"distance" : 660,  "type":"maritimo",    "time": (6.6 * maritimo["timekm"]    + 2 * maritimo["timeload"]),    "price": (6 * maritimo["euroskm"])}),    sansebastian),

            Relationship(madrid, ("Connected", {"distance" : 600,  "type":"carretera",   "time": (6   * carretera["timekm"]   + 2 * carretera["timeload"]),   "price": (6 * carretera["euroskm"])}),   santiago),
            Relationship(madrid, ("Connected", {"distance" : 600,  "type":"ferrocarril", "time": (6   * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), santiago),
            Relationship(madrid, ("Connected", {"distance" : 600,  "type":"aereo",       "time": (6   * aereo["timekm"]       + 2 * aereo["timeload"]),       "price": (6 * aereo["euroskm"])}),       santiago),
            Relationship(madrid, ("Connected", {"distance" : 620,  "type":"ferrocarril", "time": (6.2 * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), barcelona),
            Relationship(madrid, ("Connected", {"distance" : 620,  "type":"aereo",       "time": (6.2 * aereo["timekm"]       + 2 * aereo["timeload"]),       "price": (6 * aereo["euroskm"])}),       barcelona),
            Relationship(madrid, ("Connected", {"distance" : 540,  "type":"carretera",   "time": (5.4 * carretera["timekm"]   + 2 * carretera["timeload"]),   "price": (6 * carretera["euroskm"])}),   sevilla),
            Relationship(madrid, ("Connected", {"distance" : 540,  "type":"ferrocarril", "time": (5.4 * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), sevilla),
            Relationship(madrid, ("Connected", {"distance" : 450,  "type":"carretera",   "time": (4.5 * carretera["timekm"]   + 2 * carretera["timeload"]),   "price": (6 * carretera["euroskm"])}),   sansebastian),
            Relationship(madrid, ("Connected", {"distance" : 450,  "type":"aereo",       "time": (4.5 * aereo["timekm"]       + 2 * aereo["timeload"]),       "price": (6 * aereo["euroskm"])}),       sansebastian),

            Relationship(barcelona, ("Connected", {"distance" : 1100, "type":"ferrocarril", "time": (11  * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), santiago),
            Relationship(barcelona, ("Connected", {"distance" : 1100, "type":"aereo",       "time": (11  * aereo["timekm"]       + 2 * aereo["timeload"]),       "price": (6 * aereo["euroskm"])}),       santiago),
            Relationship(barcelona, ("Connected", {"distance" : 1100, "type":"maritimo",    "time": (11  *  maritimo["timekm"]    + 2 * maritimo["timeload"]),    "price": (6 * maritimo["euroskm"])}),    santiago),
            Relationship(barcelona, ("Connected", {"distance" : 620,  "type":"ferrocarril", "time": (6.2 * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), madrid),
            Relationship(barcelona, ("Connected", {"distance" : 620,  "type":"aereo",       "time": (6.2 * aereo["timekm"]       + 2 * aereo["timeload"]),       "price": (6 * aereo["euroskm"])}),       madrid),
            Relationship(barcelona, ("Connected", {"distance" : 1000, "type":"ferrocarril", "time": (10  * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), sevilla),
            Relationship(barcelona, ("Connected", {"distance" : 1000, "type":"maritimo",    "time": (10  * maritimo["timekm"]    + 2 * maritimo["timeload"]),    "price": (6 * maritimo["euroskm"])}),    sevilla),
            Relationship(barcelona, ("Connected", {"distance" : 570,  "type":"aereo",       "time": (5.7 * aereo["timekm"]       + 2 * aereo["timeload"]),       "price": (6 * aereo["euroskm"])}),       sansebastian),
            Relationship(barcelona, ("Connected", {"distance" : 570,  "type":"maritimo",    "time": (5.7 * maritimo["timekm"]    + 2 * maritimo["timeload"]),     "price": (6 * maritimo["euroskm"])}),   sansebastian),

            Relationship(sevilla, ("Connected", {"distance" : 810,  "type":"carretera",   "time": (8.1 * carretera["timekm"]   + 2 * carretera["timeload"]),   "price": (6 * carretera["euroskm"])}),   santiago),
            Relationship(sevilla, ("Connected", {"distance" : 810,  "type":"ferrocarril", "time": (8.1 * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), santiago),
            Relationship(sevilla, ("Connected", {"distance" : 810,  "type":"maritimo",    "time": (8.1 * maritimo["timekm"]    + 2 * maritimo["timeload"]),    "price": (6 * maritimo["euroskm"])}),    santiago),
            Relationship(sevilla, ("Connected", {"distance" : 540,  "type":"carretera",   "time": (5.4 * carretera["timekm"]   + 2 * carretera["timeload"]),   "price": (6 * carretera["euroskm"])}),   madrid),
            Relationship(sevilla, ("Connected", {"distance" : 540,  "type":"ferrocarril", "time": (5.4 * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), madrid),
            Relationship(sevilla, ("Connected", {"distance" : 1000, "type":"ferrocarril", "time": (10  * ferrocarril["timekm"] + 2 * ferrocarril["timeload"]), "price": (6 * ferrocarril["euroskm"])}), barcelona),
            Relationship(sevilla, ("Connected", {"distance" : 1000, "type":"maritimo",    "time": (10  * maritimo["timekm"]    + 2 * maritimo["timeload"]),    "price": (6 * maritimo["euroskm"])}),    barcelona),
            Relationship(sevilla, ("Connected", {"distance" : 920,  "type":"carretera",   "time": (9.2 * carretera["timekm"]   + 2 * carretera["timeload"]),   "price": (6 * carretera["euroskm"])}),   sansebastian),
            Relationship(sevilla, ("Connected", {"distance" : 920,  "type":"maritimo",    "time": (9.2 * maritimo["timekm"]    + 2 * maritimo["timeload"]),    "price": (6 * maritimo["euroskm"])}),    sansebastian),

            Relationship(sansebastian, ("Connected", {"distance" : 660,  "type":"carretera", "time": (6.6 * carretera["timekm"] + 2 * carretera["timeload"]), "price": (6 * carretera["euroskm"])}), santiago),
            Relationship(sansebastian, ("Connected", {"distance" : 660,  "type":"aereo",     "time": (6.6 * aereo["timekm"]     + 2 * aereo["timeload"]),     "price": (6 * aereo["euroskm"])}),     santiago),
            Relationship(sansebastian, ("Connected", {"distance" : 660,  "type":"maritimo",  "time": (6.6 * maritimo["timekm"]  + 2 * maritimo["timeload"]),  "price": (6 * maritimo["euroskm"])}),  santiago),
            Relationship(sansebastian, ("Connected", {"distance" : 450,  "type":"carretera", "time": (4.5 * carretera["timekm"] + 2 * carretera["timeload"]), "price": (6 * carretera["euroskm"])}), madrid),
            Relationship(sansebastian, ("Connected", {"distance" : 450,  "type":"aereo",     "time": (4.5 * aereo["timekm"]     + 2 * aereo["timeload"]),     "price": (6 * aereo["euroskm"])}),     madrid),
            Relationship(sansebastian, ("Connected", {"distance" : 570,  "type":"aereo",     "time": (5.7 * aereo["timekm"]     + 2 * aereo["timeload"]),     "price": (6 * aereo["euroskm"])}),     barcelona),
            Relationship(sansebastian, ("Connected", {"distance" : 570,  "type":"maritimo",  "time": (5.7 * maritimo["timekm"]  + 2 * maritimo["timeload"]),  "price": (6 * maritimo["euroskm"])}),  barcelona),
            Relationship(sansebastian, ("Connected", {"distance" : 920,  "type":"carretera", "time": (9.2 * carretera["timekm"] + 2 * carretera["timeload"]), "price": (6 * carretera["euroskm"])}), sevilla),
            Relationship(sansebastian, ("Connected", {"distance" : 920,  "type":"maritimo",  "time": (9.2 * maritimo["timekm"]  + 2 * maritimo["timeload"]),  "price": (6 * maritimo["euroskm"])}),  sevilla),

            Relationship(santiago, "Transports", carretera),
            Relationship(santiago, "Transports", ferrocarril),
            Relationship(santiago, "Transports", aereo),
            Relationship(santiago, "Transports", maritimo),
            Relationship(santiago, "Serves", urgentedia),
            Relationship(santiago, "Serves", urgentemadrugadores),
            Relationship(santiago, "Serves", urgenteprimerahora),
            Relationship(santiago, "Serves", normal),
            Relationship(santiago, "Serves", economico),

            Relationship(madrid, "Transports", carretera),
            Relationship(madrid, "Transports", ferrocarril),
            Relationship(madrid, "Transports", aereo),
            Relationship(madrid, "Service", urgentedia),
            Relationship(madrid, "Service", urgentemadrugadores),
            Relationship(madrid, "Service", urgenteprimerahora),
            Relationship(madrid, "Service", normal),
            Relationship(madrid, "Service", economico),

            Relationship(barcelona, "Transports", ferrocarril),
            Relationship(barcelona, "Transports", aereo),
            Relationship(barcelona, "Transports", maritimo),
            Relationship(barcelona, "Serves", urgentedia),
            Relationship(barcelona, "Serves", urgentemadrugadores),
            Relationship(barcelona, "Serves", urgenteprimerahora),
            Relationship(barcelona, "Serves", normal),
            Relationship(barcelona, "Serves", economico),

            Relationship(sevilla, "Transports", carretera),
            Relationship(sevilla, "Transports", ferrocarril),
            Relationship(sevilla, "Transports", maritimo),
            Relationship(sevilla, "Serves", urgentedia),
            Relationship(sevilla, "Serves", urgentemadrugadores),
            Relationship(sevilla, "Serves", urgenteprimerahora),
            Relationship(sevilla, "Serves", normal),
            Relationship(sevilla, "Serves", economico),

            Relationship(sansebastian, "Transports", carretera),
            Relationship(sansebastian, "Transports", aereo),
            Relationship(sansebastian, "Transports", maritimo),
            Relationship(sansebastian, "Serves", urgentedia),
            Relationship(sansebastian, "Serves", urgentemadrugadores),
            Relationship(sansebastian, "Serves", urgenteprimerahora),
            Relationship(sansebastian, "Serves", normal),
            Relationship(sansebastian, "Serves", economico))

        print "DB Initialized"

    def deleteAll(self):
        self.client.delete_all()
        print "DB Flushed"

    def reset(self):
        self.deleteAll()
        self.initialize()

    def getAll(self):
        return self.client.cypher.execute("match n return n")

if __name__ == "__main__":
    print "Connector"
    #c = Connector()
    #c.initialize()
    #c.deleteAll()
    #c.reset()
    #c.getAll()


