__author__ = 'Strinnityk'

from Connector import Connector
from py2neo import Node

class Package:

    ids = 0

    @staticmethod
    def createPackage(origin, destination, service):
        conn = Connector()
        statement = "match (c:City{name:'%s'})-[r]-(d:City{name:'%s'}) return r"%(origin, destination)
        data = conn.client.cypher.execute_one(statement)

        package = Node("Package")
        package["id"] = Package.ids
        package["origin"] = origin
        package["destination"] = destination
        package["service"] = service
        package["paid"] = False
        package["time"] = data["time"]
        package["price"] = data["price"]
        package["eta"] = 0 #No se puede saber cuanto lleva un nodo creado
        conn.client.create(package)

        Package.ids += 1
        return package

    @staticmethod
    def getPackageLocation(id):
        conn = Connector()
        statement = statement = "match (p:Package) where p.id=%d return p.origin"%(id)
        return conn.client.cypher.execute_one(statement)

    @staticmethod
    def pay(origin, destination, service, id):
        conn = Connector()
        statement = statement = "match (p:Package) where p.origin='%s' and p.destination='%s' and p.service='%s' and p.id=%d return p"%(origin, destination, service, id)
        package = conn.client.cypher.execute_one(statement)
        if package:
            package["paid"] = True
            package.push()

if __name__ == "__main__":
    print "Package"
    #Package.createPackage("Santiago", "Madrid", "normal")
    #print Package.getPackageLocation(0)
    #Package.pay("Santiago", "Madrid", "normal", 0)